<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_fach',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'enablecolumns' => [
        ],
        'searchFields' => 'name,orte',
        'iconfile' => 'EXT:akademie_ruhr_studienorte/Resources/Public/Icons/tx_akademieruhrstudienorte_domain_model_fach.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'name, orte',
    ],
    'types' => [
        '1' => ['showitem' => 'name, orte'],
    ],
    'columns' => [

        'name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_fach.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'orte' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_fach.orte',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_akademieruhrstudienorte_domain_model_ort',
                'MM' => 'tx_akademieruhrstudienorte_fach__mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
            
        ],
    
    ],
];
