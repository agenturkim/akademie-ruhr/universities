<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_abschluss',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'enablecolumns' => [
        ],
        'searchFields' => 'name',
        'iconfile' => 'EXT:akademie_ruhr_studienorte/Resources/Public/Icons/tx_akademieruhrstudienorte_domain_model_abschluss.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'name',
    ],
    'types' => [
        '1' => ['showitem' => 'name'],
    ],
    'columns' => [

        'name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_abschluss.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
    
    ],
];
