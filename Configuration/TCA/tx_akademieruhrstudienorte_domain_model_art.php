<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_art',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'enablecolumns' => [
        ],
        'searchFields' => 'name,kurzform',
        'iconfile' => 'EXT:akademie_ruhr_studienorte/Resources/Public/Icons/tx_akademieruhrstudienorte_domain_model_art.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'name, kurzform',
    ],
    'types' => [
        '1' => ['showitem' => 'name, kurzform'],
    ],
    'columns' => [

        'name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_art.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'kurzform' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_art.kurzform',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
    
    ],
];
