<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_ort',
        'label' => 'bezeichnung',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'enablecolumns' => [
        ],
        'searchFields' => 'bezeichnung,webseite,art,stadt',
        'iconfile' => 'EXT:akademie_ruhr_studienorte/Resources/Public/Icons/tx_akademieruhrstudienorte_domain_model_ort.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'bezeichnung, webseite, art, stadt',
    ],
    'types' => [
        '1' => ['showitem' => 'bezeichnung, webseite, art, stadt'],
    ],
    'columns' => [

        'bezeichnung' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_ort.bezeichnung',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'webseite' => [
            'exclude' => false,
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_ort.webseite',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'art' => [
            'exclude' => true,
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_ort.art',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_akademieruhrstudienorte_domain_model_art',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'stadt' => [
            'exclude' => true,
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_ort.stadt',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_akademieruhrstudienorte_domain_model_stadt',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
    
    ],
];
