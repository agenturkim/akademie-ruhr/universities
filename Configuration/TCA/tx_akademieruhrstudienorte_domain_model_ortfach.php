<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_ortfach',
        'label' => 'fach',
        'label_alt' => 'ort',
        'label_alt_force' => 1,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'enablecolumns' => [],
        'searchFields' => 'fach,ort,abschluesse',
        'iconfile' => 'EXT:akademie_ruhr_studienorte/Resources/Public/Icons/tx_akademieruhrstudienorte_domain_model_ortfach.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'detailpid, fach, ort, abschluesse'
    ],
    'types' => [
        '1' => ['showitem' => 'detailpid, fach, ort, abschluesse'],
    ],
    'columns' => [

        'detailpid' => [
            'label' => 'Detailseite',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages',
                'maxitems' => 1,
                'size' => 1
            ]
        ],

        'fach' => [
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_ortfach.fach',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_akademieruhrstudienorte_domain_model_fach',
                'minitems' => 1,
                'maxitems' => 1,
            ],
        ],
        'ort' => [
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_ortfach.ort',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_akademieruhrstudienorte_domain_model_ort',
                'minitems' => 1,
                'maxitems' => 1,
            ],
        ],
        'abschluesse' => [
            'label' => 'LLL:EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_db.xlf:tx_akademieruhrstudienorte_domain_model_ortfach.abschluesse',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_akademieruhrstudienorte_domain_model_abschluss',
                'MM' => 'tx_akademieruhrstudienorte_ortfach_abschluss_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],

        ],
    ]
];
