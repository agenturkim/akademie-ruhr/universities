<?php

$EM_CONF[$_EXTKEY] = [
    'title' => '[Akademie Ruhr] Studienorte',
    'description' => 'Studienorte der Akademie Ruhr',
    'category' => 'plugin',
    'author' => 'Pascale Beier',
    'author_email' => 'mail@pascalebeier.de',
    'state' => 'stable',
    'version' => '1.0.7',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
