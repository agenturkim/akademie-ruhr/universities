#
# Table structure for table 'tx_akademieruhrstudienorte_domain_model_ort'
#
CREATE TABLE tx_akademieruhrstudienorte_domain_model_ort (
	bezeichnung varchar(255) DEFAULT '' NOT NULL,
	webseite varchar(255) DEFAULT '' NOT NULL,
	art int(11) unsigned DEFAULT '0',
	stadt int(11) unsigned DEFAULT '0',
);

#
# Table structure for table 'tx_akademieruhrstudienorte_domain_model_abschluss'
#
CREATE TABLE tx_akademieruhrstudienorte_domain_model_abschluss (
	name varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_akademieruhrstudienorte_domain_model_art'
#
CREATE TABLE tx_akademieruhrstudienorte_domain_model_art (
	name varchar(255) DEFAULT '' NOT NULL,
	kurzform varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_akademieruhrstudienorte_domain_model_fach'
#
CREATE TABLE tx_akademieruhrstudienorte_domain_model_fach (
	name varchar(255) DEFAULT '' NOT NULL,
	orte int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tx_akademieruhrstudienorte_domain_model_ortfach'
#
CREATE TABLE tx_akademieruhrstudienorte_domain_model_ortfach (
	detailpid int(11) DEFAULT '0' NOT NULL,
	fach int(11) unsigned DEFAULT '0',
	ort int(11) unsigned DEFAULT '0',
	abschluesse int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tx_akademieruhrstudienorte_domain_model_stadt'
#
CREATE TABLE tx_akademieruhrstudienorte_domain_model_stadt (
	name varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_akademieruhrstudienorte_fach__mm'
#
CREATE TABLE tx_akademieruhrstudienorte_fach__mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_akademieruhrstudienorte_ortfach_abschluss_mm'
#
CREATE TABLE tx_akademieruhrstudienorte_ortfach_abschluss_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

