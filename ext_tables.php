<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'RuhrConnect.AkademieRuhrStudienorte',
            'Pi1',
            'Studienorte'
        );

        $pluginSignature = str_replace('_', '', 'akademie_ruhr_studienorte') . '_pi1';
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:akademie_ruhr_studienorte/Configuration/FlexForms/flexform_pi1.xml');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('akademie_ruhr_studienorte', 'Configuration/TypoScript', 'Akademie Ruhr Studienorte');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_akademieruhrstudienorte_domain_model_ort', 'EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_csh_tx_akademieruhrstudienorte_domain_model_ort.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_akademieruhrstudienorte_domain_model_ort');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_akademieruhrstudienorte_domain_model_abschluss', 'EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_csh_tx_akademieruhrstudienorte_domain_model_abschluss.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_akademieruhrstudienorte_domain_model_abschluss');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_akademieruhrstudienorte_domain_model_art', 'EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_csh_tx_akademieruhrstudienorte_domain_model_art.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_akademieruhrstudienorte_domain_model_art');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_akademieruhrstudienorte_domain_model_fach', 'EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_csh_tx_akademieruhrstudienorte_domain_model_fach.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_akademieruhrstudienorte_domain_model_fach');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_akademieruhrstudienorte_domain_model_ortfach', 'EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_csh_tx_akademieruhrstudienorte_domain_model_ortfach.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_akademieruhrstudienorte_domain_model_ortfach');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_akademieruhrstudienorte_domain_model_stadt', 'EXT:akademie_ruhr_studienorte/Resources/Private/Language/locallang_csh_tx_akademieruhrstudienorte_domain_model_stadt.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_akademieruhrstudienorte_domain_model_stadt');

    }
);
