<?php
defined('TYPO3_MODE') || die('Access denied.');


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'RuhrConnect.AkademieRuhrStudienorte',
    'Pi1',
    [
        'OrtFach' => 'list,listByOrt,listByFach,filter'
    ]
);
