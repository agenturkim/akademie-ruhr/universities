<?php
namespace RuhrConnect\AkademieRuhrStudienorte\Domain\Model;

/***
 *
 * This file is part of the "Akademie Ruhr Studienorte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Pascale Beier <p.beier@ruhr-connect.de>, ruhr-connect GmbH
 *           Andreas Wietfeld <a.wietfeld@ruhr-connect.de>, ruhr-connect GmbH
 *
 ***/

/**
 * OrtFach
 */
class OrtFach extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * @var int
     */
    protected $detailpid;

    /**
     * fach
     * 
     * @var \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Fach
     */
    protected $fach = null;

    /**
     * ort
     * 
     * @var \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort
     */
    protected $ort = null;

    /**
     * Verfügbare Abschlüsse einer Ort/Fach-Kombination
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Abschluss>
     */
    protected $abschluesse = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->abschluesse = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Abschluss
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Abschluss $abschluesse
     * @return void
     */
    public function addAbschluesse(\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Abschluss $abschluesse)
    {
        $this->abschluesse->attach($abschluesse);
    }

    /**
     * Removes a Abschluss
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Abschluss $abschluesseToRemove The Abschluss to be removed
     * @return void
     */
    public function removeAbschluesse(\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Abschluss $abschluesseToRemove)
    {
        $this->abschluesse->detach($abschluesseToRemove);
    }

    /**
     * Returns the abschluesse
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Abschluss> $abschluesse
     */
    public function getAbschluesse()
    {
        return $this->abschluesse;
    }

    /**
     * Sets the abschluesse
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Abschluss> $abschluesse
     * @return void
     */
    public function setAbschluesse(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $abschluesse)
    {
        $this->abschluesse = $abschluesse;
    }

    /**
     * Returns the fach
     * 
     * @return \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Fach $fach
     */
    public function getFach()
    {
        return $this->fach;
    }

    /**
     * Sets the fach
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Fach $fach
     * @return void
     */
    public function setFach(\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Fach $fach)
    {
        $this->fach = $fach;
    }

    /**
     * Returns the ort
     * 
     * @return \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort $ort
     */
    public function getOrt()
    {
        return $this->ort;
    }

    /**
     * Sets the ort
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort $ort
     * @return void
     */
    public function setOrt(\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort $ort)
    {
        $this->ort = $ort;
    }

    public function setDetailpid(int $detailpid) 
    {
        $this->detailpid = $detailpid;
    }

    public function getDetailpid(): int
    {
        return $this->detailpid;
    }
}
