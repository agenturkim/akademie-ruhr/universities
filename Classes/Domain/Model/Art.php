<?php
namespace RuhrConnect\AkademieRuhrStudienorte\Domain\Model;

/***
 *
 * This file is part of the "Akademie Ruhr Studienorte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Pascale Beier <p.beier@ruhr-connect.de>, ruhr-connect GmbH
 *           Andreas Wietfeld <a.wietfeld@ruhr-connect.de>, ruhr-connect GmbH
 *
 ***/

/**
 * Art
 */
class Art extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * Art des Studienortes (FH, TU, Universität ...)
     * 
     * @var string
     */
    protected $name = '';

    /**
     * Kurzform (TU, FH ...)
     * 
     * @var string
     */
    protected $kurzform = '';

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the kurzform
     * 
     * @return string $kurzform
     */
    public function getKurzform()
    {
        return $this->kurzform;
    }

    /**
     * Sets the kurzform
     * 
     * @param string $kurzform
     * @return void
     */
    public function setKurzform($kurzform)
    {
        $this->kurzform = $kurzform;
    }
}
