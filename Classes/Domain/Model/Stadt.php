<?php
namespace RuhrConnect\AkademieRuhrStudienorte\Domain\Model;

use RuhrConnect\AkademieRuhrStudienorte\Support\FileReferenceFactory;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Core\Resource\StorageRepository;
use RuhrConnect\AkademieRuhrStudienorte\Support\Str;
use TYPO3\CMS\Core\Resource\FileInterface;


/***
 *
 * This file is part of the "Akademie Ruhr Studienorte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Pascale Beier <p.beier@ruhr-connect.de>, ruhr-connect GmbH
 *           Andreas Wietfeld <a.wietfeld@ruhr-connect.de>, ruhr-connect GmbH
 *
 ***/

/**
 * Stadt
 */
class Stadt extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * Name der Stadt, bspw. Dortmund
     * 
     * @var string
     */
    protected $name = '';

    /**
     * Pfad des zugeordneten Bildes
     * 
     * @var string
     */
    protected $imagePath = "/mappenvorbereitungskurs/studienorte/header-studienorte/mappenkurs-{ort}.jpg";

    /**
     * Platzhalter-Bild
     * 
     * @var string
     */
    protected $placeholder = "hamburg";

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getImage()
    {
        $identifier = str_replace("{ort}", $this->getName(), $this->imagePath);
        $placeholder = str_replace("{ort}", $this->placeholder, $this->imagePath);

        // Generate url safe string
        $identifier = Str::slug($identifier);

        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $storageRepository = $objectManager->get(StorageRepository::class);

        $storage = $storageRepository->findByUid(1);

        if ($storage->hasFile($identifier)) {
            return $storage->getFile($identifier);
        }

        // Platzhalter ausgeben falls das Stadtbild nicht existiert
        return $storage->getFile($placeholder);

    }

}
