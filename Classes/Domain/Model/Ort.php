<?php
namespace RuhrConnect\AkademieRuhrStudienorte\Domain\Model;

/***
 *
 * This file is part of the "Akademie Ruhr Studienorte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Pascale Beier <p.beier@ruhr-connect.de>, ruhr-connect GmbH
 *           Andreas Wietfeld <a.wietfeld@ruhr-connect.de>, ruhr-connect GmbH
 *
 ***/

/**
 * Ort
 */
class Ort extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * Bezeichnung des Studienorts
     * 
     * @var string
     */
    protected $bezeichnung = '';

    /**
     * Webseite des Studienorts
     * 
     * @var string
     */
    protected $webseite = '';

    /**
     * art
     * 
     * @var \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Art
     */
    protected $art = null;

    /**
     * Zugeordnete Stadt
     * 
     * @var \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Stadt
     */
    protected $stadt = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->stadt->initStorageObjects();
    }

    /**
     * Returns the webseite
     * 
     * @return string $webseite
     */
    public function getWebseite()
    {
        return $this->webseite;
    }

    /**
     * Sets the webseite
     * 
     * @param string $webseite
     * @return void
     */
    public function setWebseite($webseite)
    {
        $this->webseite = $webseite;
    }

    /**
     * Returns the art
     * 
     * @return \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Art $art
     */
    public function getArt()
    {
        return $this->art;
    }

    /**
     * Sets the art
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Art $art
     * @return void
     */
    public function setArt(\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Art $art)
    {
        $this->art = $art;
    }

    /**
     * Returns the bezeichnung
     * 
     * @return string $bezeichnung
     */
    public function getBezeichnung()
    {
        return $this->bezeichnung;
    }

    /**
     * Sets the bezeichnung
     * 
     * @param string $bezeichnung
     * @return void
     */
    public function setBezeichnung($bezeichnung)
    {
        $this->bezeichnung = $bezeichnung;
    }

    /**
     * Returns the stadt
     * 
     * @return \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Stadt $stadt
     */
    public function getStadt()
    {
        return $this->stadt;
    }

    /**
     * Sets the stadt
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Stadt $stadt
     * @return void
     */
    public function setStadt(\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Stadt $stadt)
    {
        $this->stadt = $stadt;
    }
}
