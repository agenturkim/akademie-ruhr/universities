<?php
namespace RuhrConnect\AkademieRuhrStudienorte\Domain\Model;

/***
 *
 * This file is part of the "Akademie Ruhr Studienorte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Pascale Beier <p.beier@ruhr-connect.de>, ruhr-connect GmbH
 *           Andreas Wietfeld <a.wietfeld@ruhr-connect.de>, ruhr-connect GmbH
 *
 ***/

/**
 * Fach
 */
class Fach extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * Name des Fachs, bspw. Produktdesign
     * 
     * @var string
     */
    protected $name = '';

    /**
     * Zugeordnete Studienorte
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort>
     */
    protected $orte = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->orte = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Adds a
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort $orte
     * @return void
     */
    public function addOrte($orte)
    {
        $this->orte->attach($orte);
    }

    /**
     * Removes a
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort $orteToRemove The Ort to be removed
     * @return void
     */
    public function removeOrte($orteToRemove)
    {
        $this->orte->detach($orteToRemove);
    }

    /**
     * Returns the orte
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort> orte
     */
    public function getOrte()
    {
        return $this->orte;
    }

    /**
     * Sets the orte
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort> $orte
     * @return void
     */
    public function setOrte(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $orte)
    {
        $this->orte = $orte;
    }
}
