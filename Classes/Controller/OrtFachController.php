<?php
namespace RuhrConnect\AkademieRuhrStudienorte\Controller;

use RuhrConnect\AkademieRuhrStudienorte\Domain\Repository\OrtFachRepository;
use RuhrConnect\AkademieRuhrStudienorte\Domain\Repository\OrtRepository;
use RuhrConnect\AkademieRuhrStudienorte\Domain\Repository\FachRepository;
use RuhrConnect\AkademieRuhrStudienorte\Domain\Repository\AbschlussRepository;
use RuhrConnect\AkademieRuhrStudienorte\Domain\Repository\StadtRepository;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;


/***
 *
 * This file is part of the "Akademie Ruhr Studienorte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Pascale Beier <p.beier@ruhr-connect.de>, ruhr-connect GmbH
 *           Andreas Wietfeld <a.wietfeld@ruhr-connect.de>, ruhr-connect GmbH
 *
 ***/

/**
 * OrtFachController
 */
class OrtFachController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /** @var OrtFachRepository */
    protected $ortFachRepository = null;

    /** @var OrtRepository */
    protected $ortRepository = null;

    /** @var FachRepository */
    protected $fachRepository = null;

    /** @var StadtRepository */
    protected $stadtRepository = null;

    /**
     * @param OrtFachRepository $ortFachRepository
     */
    public function injectOrtFachRepository(OrtFachRepository $ortFachRepository)
    {
        $this->ortFachRepository = $ortFachRepository;
    }

    /**
     * @param OrtRepository $ortRepository
     */
    public function injectOrtRepository(OrtRepository $ortRepository)
    {
        $this->ortRepository = $ortRepository;
    }

    /**
     * @param FachRepository $fachRepository
     */
    public function injectFachRepository(FachRepository $fachRepository)
    {
        $this->fachRepository = $fachRepository;
    }
  
    /**
     * @param StadtRepository $stadtRepository
     */
    public function injectStadtRepository(StadtRepository $stadtRepository)
    {
        $this->stadtRepository = $stadtRepository;
    }

    /**
     * Ausgabe aller Ort/Fach-Kombinationen
     * 
     * @return void
     */
    public function listAction()
    {
        $this->view->assign('locations', $this->ortFachRepository->findAll());
    }

    /**
     * OrtFach-Kombinations-Liste über die Angabe eines Ortes.
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort $ort
     * @return void
     */
    public function listByOrtAction(\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Ort $ort = null)
    {
        if ($ort === null) {
            $setting = 'singleOrt';
            $ortId = (int) $this->settings[$setting] > 0 ? $this->settings[$setting] : 0;
            $ort = $this->ortRepository->findByUid($ortId);
        } else {
            $ortId = $ort->getUid();
        }

        $this->view->assign('city', $ort->getStadt()->getName());
        $this->view->assign('locations', $this->ortFachRepository->findByOrt($ortId));
    }

    /**
     * OrtFach-Kombinations-Liste über die Angabe eines Fachs.
     * 
     * @param \RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Fach $fach
     * @return void
     */
    public function listByFachAction(\RuhrConnect\AkademieRuhrStudienorte\Domain\Model\Fach $fach = null)
    {
        if ($fach === null) {
            $setting = 'singleFach';
            $fachId = (int) $this->settings[$setting] > 0 ? $this->settings[$setting] : 0;
            $fach =  $this->fachRepository->findByUid($fachId);
        } else {
            $fachId = $fach->getUid();
        }

        $this->view->assignMultiple([
            'field' => $fach->getName(),
            'locations' =>  $this->ortFachRepository->findByFach($fachId),
        ]);
    }

    public function filterAction()
    {
        $this->view->assignMultiple([
            'cities' => $this->stadtRepository->findAll(),
            'fields' => $this->fachRepository->findAll()
        ]);
    }
}
