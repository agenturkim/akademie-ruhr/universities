<?php
namespace RuhrConnect\AkademieRuhrStudienorte\Support;

/***
 *
 * This file is part of the "Akademie Ruhr Studienorte" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Pascale Beier <p.beier@ruhr-connect.de>, ruhr-connect GmbH
 *           Andreas Wietfeld <a.wietfeld@ruhr-connect.de>, ruhr-connect GmbH
 *
 ***/

final class Str
{
    const UMLAUTS = [
        "ä" => "ae",
        "ö" => "oe",
        "ü" => "ue",
        "ß" => "ss",
    ];

    /**
     * Adapted from Illuminate/Support/Str
     * 
     * @link https://github.com/laravel/framework/blob/9f313ce9bb5ad49a06ae78d33fbdd1c92a0e21f6/src/Illuminate/Support/Str.php#L418
     * 
     * @param string $title Input String
     * @return string
     */
    public static function slug(string $title, string $separator = '-'): string
    {
        $title = static::removeUmlauts(mb_strtolower($title));

        // Convert all dashes/underscores into separator
        $flip = $separator == '-' ? '_' : '-';
        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);
        
        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);

        return trim($title, $separator);
    }

    public static function removeUmlauts(string $string): string
    {
        foreach (self::UMLAUTS as $search => $replace) {
           $string = str_replace($search, $replace, $string);
        }

        return $string;
    }
}